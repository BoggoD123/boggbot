import discord
import asyncio
from discord.ext.commands import Bot
from discord.ext import commands
import platform



#Bot prefix, etc
client = Bot(description="Spooky", command_prefix="b!", pm_help = False)

# Bot startup
@client.event
async def on_ready():
	print(client.user.name+' │ ID:'+client.user.id+') │ Connected to '+str(len(client.servers))+' servers │ Connected to '+str(len(set(client.get_all_members())))+' users')
	print('Discord.py Version: %s │ Python Version: %s' % (discord.__version__, platform.python_version()))
	return await client.change_presence(game=discord.Game(name="test")) #Set bots game on startup, can be changed using the status command

#Insert IDs here to add admins
devList = ("163414553764298753")

#Stop bot responding to other bots
@client.event
async def on_message(message):
	if message.author.bot: 
		return
	
	await client.process_commands(message) 
#End


#Commands

#Posts a laughtrack
@client.command(pass_context=True)
async def laughtrack(ctx):
	file = open('audio\Hilarious.mp3', 'rb')
	await client.send_file(ctx.message.channel, file, content=ctx.message.author.mention)
	file.close()
#End
	
	
#Lets anyone who has an id in the devList change the bot's playing status
@client.command(pass_context=True)
async def status(ctx, status):
	if ctx.message.author.id in devList:
		try:
			if len(status) >= 129:
				await client.say("%s, an error occured: `Playing status cannot be above 128 characters.`" % ctx.message.author.mention)
			else:
				await client.change_presence(game=discord.Game(name=str(status)))
				await client.say("%s Okay, status changed to `%s`" % (ctx.message.author.mention, status))
		except Exception as e:
			await client.say("%s, an error occured: `%s`" % (ctx.message.author.mention, e))
	else:
		await client.say(":no_entry: │ You do not have permission to use this command.")
#End

#Bot repeats a certain phrase (Usage <Phrase> <Repeat 1-25> <Delay 5+>
@client.command(pass_context=True)
async def botsay(ctx, input="", amount="", delay=""):
	try:
		if input == "" or amount == "" or delay == "":
			await client.say("%s, an error occured: `Missing arguments.`" % ctx.message.author.mention)
		elif int(amount) >= 26 or int(delay) <= 4 or int(amount) <= 0:
			await client.say("Please enter no more that 25 messages, and a minimum of 5 second delay.")
		else:
			await client.say('%s Okay, I will say "%s" %s times, with a delay of %s seconds' % (ctx.message.author.mention, input, amount, delay))
			for testvar2 in range(0, int(amount)):
				await client.say(str(input))
				await asyncio.sleep(int(delay))
	except Exception as e:
		await client.say("%s, an error occured: `%s`" % (ctx.message.author.mention, e))
#End

#Lets server admins change the bots nickname.
@client.command(pass_context=True)
async def botnick(ctx, name='BoggBot'):
	if ctx.message.author.server_permissions.administrator == False:
		await client.say(":no_entry: │ You do not have permission to use this command.")
	else:
		try:
			if len(name) >= 33:
				await client.say("%s, an error occured: `Must be 32 or fewer in length`" % ctx.message.author.mention)
			else:
				botUser = ctx.message.server.me
				await client.change_nickname(botUser, name)
				await client.say("%s Okay, nickname set to `%s`" % (ctx.message.author.mention, name))
		except Exception as e:
			await client.say("%s, an error occured: `%s`" % (ctx.message.author.mention, e))
			print("%s, an error occured: `%s`" % (ctx.message.author.mention, e))
#End
		
#Gives information about the caller of the command (NOTE: Timestamp of the message will use your PCs timezone. Change "GMT" as needed)
@client.command(pass_context=True)
async def whoami(ctx):
	await client.say("User id: `%s` \nUser name: `@%s` \nCommand used: `%s GMT`" % (ctx.message.author.id, ctx.message.author, ctx.message.timestamp))
#End
		
#End of commands
	
	
client.run('Put your bot token here!')